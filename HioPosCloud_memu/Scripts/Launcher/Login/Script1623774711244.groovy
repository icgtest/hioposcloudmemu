import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import internal.GlobalVariable as GlobalVariable
import utils.Common as Common

Mobile.waitForElementPresent(findTestObject('Pantalla Login/android.widget.TextView.Identificacin'), 10, FailureHandling.STOP_ON_FAILURE)
KeywordUtil.logInfo('Pantalla de Login encontrada')
Mobile.setText(findTestObject('Pantalla Login/android.widget.EditText.Usuario'), GlobalVariable.user, 1)
Mobile.setText(findTestObject('Pantalla Login/android.widget.EditText.Password'), GlobalVariable.password, 1)
Common.tapToPosition(GlobalVariable.XY_BarraSuperior_Continuar)
Thread.sleep(3000)//esperamos alerta de sustitución de terminal
Common.tapToPosition(GlobalVariable.XY_SUSTITUIR)

