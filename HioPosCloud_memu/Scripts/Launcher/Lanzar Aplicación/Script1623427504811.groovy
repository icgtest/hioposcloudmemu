import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable
import io.appium.java_client.AppiumDriver
import utils.Common
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
//TestObject prova2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[text() = 'Vendedor 1']"); MANEL, ES UNA PRUEBA.


int TIPOINICIO_IDIOMAS=1,TIPOINICIO_VENDEDORES=2

Mobile.startExistingApplication('icg.android.start', FailureHandling.STOP_ON_FAILURE)
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

int tipoInicio=detectarTipoInicio()

switch (tipoInicio) {
	case TIPOINICIO_IDIOMAS:
		Mobile.tap(findTestObject('Pantalla Idiomas/view_idiomas_espanol'), 10)
        Mobile.callTestCase(findTestCase('Launcher/Login'), [('user') : GlobalVariable.user, ('password') : GlobalVariable.password], FailureHandling.STOP_ON_FAILURE)		
		aceptarContrato()
		completarAsistente()
		break
	case TIPOINICIO_VENDEDORES:
		//TODO
		break	
	break
}

//driver.resetApp()

//Detecta en que pantalla arranca la app.
def detectarTipoInicio() {
	// Me espero hasta que tenga cargada la app
	Mobile.waitForElementPresent(findTestObject('Comunes/android.view.BarraMenuSuperior'), 10, FailureHandling.STOP_ON_FAILURE)
		
	//Identifico las tres posibles pantallas que me puedo encontrar
	 if (Mobile.waitForElementPresent(findTestObject('Pantalla Vendedores/android.view.SellerPageViewer'), 3, FailureHandling.CONTINUE_ON_FAILURE)) {
		KeywordUtil.logInfo("Pantalla de Vendedores encontrada ")
		return 2
	}
	//No queda claro de si este escenario es posible, pantalla de login sin pasar por pantalla de idiomas...
	else if (Mobile.waitForElementPresent(findTestObject('Pantalla Login/android.widget.TextView.Identificacin'),3,FailureHandling.CONTINUE_ON_FAILURE)) {
		KeywordUtil.logInfo("Pantalla de Login encontrada")
		return 3
	}
	
	else if (Mobile.waitForElementPresent(findTestObject('Pantalla Idiomas/android.widget.ImageViewHioPos'),3,FailureHandling.CONTINUE_ON_FAILURE)) {
		KeywordUtil.logInfo("Pantalla de Idiomas encontrada")
		return 1
	}
	
	//En caso de que ninguno de las condiciones anteriores se cumpla, estoy en una pantalla desconocida.
	else {
		KeywordUtil.logInfo("Pantalla desconocida.")
		return -1
	}	
}

//Espera hasta que se muestra el webview con el contrato de las condiciones y acepta para seguir adelante.
def aceptarContrato() {
	
	if (Mobile.waitForElementPresent(findTestObject('Pantalla Contrato/android.widget.ImageView'), 5, FailureHandling.CONTINUE_ON_FAILURE)) {
		KeywordUtil.logInfo("Pantalla de Aceptación de contrato encontrada")
		Mobile.waitForElementPresent(findTestObject('Pantalla Contrato/android.webkit.WebView'), 10, FailureHandling.STOP_ON_FAILURE)
		Common.tapToPosition(GlobalVariable.XY_BarraSuperior_Continuar)
		Thread.sleep(3000)//esperamos alerta de aceptacióin de contrato.
		Common.tapToPosition(GlobalVariable.XY_AceptacionContrato_si)
	}else {
		KeywordUtil.logInfo("Pantalla de Aceptación de contrato no encontrada")
	}
	
}

//Funcional pero pendiente de añadir más verificaciones y mas posibles pantallas.
def completarAsistente() {
	//Pantalla asistente tienda
	Mobile.waitForElementPresent(findTestObject('Pantalla Asistente Tienda/android.widget.TextView - Tienda'), 10, FailureHandling.STOP_ON_FAILURE)
	KeywordUtil.logInfo("Pantalla de asistente Tienda encontrada")
	Common.tapToPosition(GlobalVariable.XY_BarraSuperior_Continuar)	
	Common.waitForElementNotVisible(findTestObject('Pantalla Dispositivos/android.widget.ImageViewsettingsImage'), 5)
	Mobile.waitForElementPresent(findTestObject('Pantalla Dispositivos/android.widget.FrameLayout'), 10, FailureHandling.STOP_ON_FAILURE)
	Thread.sleep(3000)
	Common.tapToPosition(GlobalVariable.XY_TestImpresora)
	KeywordUtil.logInfo("Test de Impresora realizado")
	Thread.sleep(3000)
	Common.tapToPosition(GlobalVariable.XY_BarraSuperior_Continuar)
	Mobile.waitForElementPresent(findTestObject('Pantalla Asistente Familias y Articulos/android.widget.TextView - FAMILIAS'), 10, FailureHandling.STOP_ON_FAILURE)
	KeywordUtil.logInfo("Pantalla de familias y artículos encontrada")
	Common.tapToPosition(GlobalVariable.XY_BarraSuperior_Continuar)
	Mobile.waitForElementPresent(findTestObject('Pantalla Asistente Felicidades/android.widget.TextView - Felicidades'), 10, FailureHandling.STOP_ON_FAILURE)
	KeywordUtil.logInfo("Pantalla final del asistente encontrada.")
	Mobile.tap(findTestObject('Pantalla Asistente Felicidades/android.view.View.Iniciar'), 3)
	Mobile.waitForElementPresent(findTestObject('Pantalla Vendedores/android.view.SellerPageViewer'), 3, FailureHandling.CONTINUE_ON_FAILURE)
	KeywordUtil.logInfo("Pantalla de Vendedores encontrada ")	
}

