package utils

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil



import internal.GlobalVariable

public class Common {

	@Keyword
	def static tapToPosition( int x, int y) {
		Mobile.tapAtPosition(x, y)
		KeywordUtil.logInfo("Tap en posición: "+x+","+y)
	}

	@Keyword
	def static tapToPosition( String xy) {
		String x= xy.split(",")[0]
		String y= xy.split(",")[1]
		tapToPosition(new Integer(x), new Integer(y))
	}

	@Keyword
	def static boolean waitForElementNotVisible(TestObject testObject, int timeout){

		while (Mobile.waitForElementPresent(testObject, timeout, FailureHandling.CONTINUE_ON_FAILURE)) {
			KeywordUtil.logInfo("waitForElementNotVisible visible")
			Thread.sleep(1000)
		}

		KeywordUtil.logInfo("waitForElementNotVisible no visible")
		return true
	}
}
